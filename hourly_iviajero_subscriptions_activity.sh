#!/bin/bash                                                                                                                                                                                     

#1=sinceDate
#2=untilDate
#3=metricPrefix
pushSubscriptorActionCount() {

	echo "procesing subscription stats between $1 and $2"	
	
	metricPrefix=$3
	
	mysql -udashboard -pd4shB04rd -hlocalhost viajerotelcel -N -e"SELECT ActionType, COUNT(id) as Count FROM subscriptor_action WHERE creationDate >= '$1' AND creationDate <= '$2' GROUP BY ActionType" | while read ActionType Count; do
		/home/santiago/lsm.dashletpusher/push_scalar_dashlet.sh $metricPrefix"_"$ActionType $Count "$2"
	done
	
}

firstOfMonth="$(date --date='today' +'%Y-%m-01 00:00:00')"
today="$(date --date='today' +'%Y-%m-%d 00:00:00')"
last60mins="$(date --date='60 mins ago' +'%Y-%m-%d %H:%M:%S')"
now="$(date +'%Y-%m-%d %H:%M:%S')"

pushSubscriptorActionCount "$last60mins" "$now" "last60mins"
pushSubscriptorActionCount "$today" "$now" "today"
pushSubscriptorActionCount "$firstOfMonth" "$now" "month"