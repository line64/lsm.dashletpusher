#!/bin/bash                                                                                                                                                                                     

#1=sinceDate
#2=untilDate
#3=metricPrefix
pushSubscriptorActionCount() {

	echo "procesing subscription stats between $1 and $2"	
	
	metricPrefix=$3
	
	mysql -udashboard -pd4shB04rd -hlocalhost viajerotelcel -N -e"SELECT ActionType, COUNT(id) as Count FROM subscriptor_action WHERE creationDate >= '$1' AND creationDate <= '$2' GROUP BY ActionType" | while read ActionType Count; do
		/home/santiago/lsm.dashletpusher/push_scalar_dashlet.sh $metricPrefix"_"$ActionType $Count "$2"
	done
	
}

for i in {12..28}
do
	ii=$((i+1))	
	pushSubscriptorActionCount "2013-1-$i 00:00:00" "2013-1-$ii 00:00:00" "yesterday"
done

for i in {0..23}
do
	ii=$((i+1))	
	pushSubscriptorActionCount "2013-1-28 $i:00:00" "2013-1-28 $ii:00:00" "last60mins"
done

for i in {0..11}
do
	ii=$((i+1))	
	pushSubscriptorActionCount "2013-1-29 $i:00:00" "2013-1-29 $ii:00:00" "last60mins"
done