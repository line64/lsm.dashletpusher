#!/bin/bash                                                                                                                                                                                     

echo "pushing to metric $1 value $2"

uri="http://dashlets.cloudpier.net/update/4f2ab6026654541564d0050b/$1";
urlTimeStamp=$(date --date="$3" +%s)	
data="scalarValue=$2&unixTimestamp=$urlTimeStamp"

echo "push data: $data"

wget --post-data $data $uri -O /dev/null