#!/bin/bash                                                                                                                                                                                     

#1=sinceDate
#2=untilDate
#3=metricPrefix
pushSubscriptorActionCount() {

	echo "procesing subscription stats between $1 and $2"	
		
	metricPrefix=$3
	
	mysql -udashboard -pd4shB04rd -hlocalhost viajerotelcel -N -e"SELECT ActionType, COUNT(id) as Count FROM subscriptor_action WHERE creationDate >= '$1' AND creationDate <= '$2' GROUP BY ActionType" | while read ActionType Count; do
		/home/santiago/lsm.dashletpusher/push_scalar_dashlet.sh $metricPrefix"_"$ActionType $Count "$2"
	done
	
}

yesterday="$(date --date='yesterday' +'%Y-%m-%d 00:00:00')"
today="$(date --date='today' +'%Y-%m-%d 00:00:00')"

pushSubscriptorActionCount "$yesterday" "$today" "yesterday"